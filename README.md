# configurator

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Описание работы
### Получение данных
Основные данные берем из файла main_data.js
1. seria - Серия
2. base - Базовая модификация с параметрами и без изменяемых длин
3. d - Диаметр поршня
4. price - цена базовой модификации
5. hod_price - стоимость 1мм удлинения штока
6. len_price - стоимость 1мм удлинения резьбы