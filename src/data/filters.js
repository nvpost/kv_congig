export let filters = [
    {
        label: 'Резьба штока',
        params: [
            {
                value: '',
                label: 'Наружная',
                text: 'При заказе не указывается'
            },
            {
                value: 'F',
                label: 'Внутренняя',
                text: ''
            },
        ],
        key: "groove",
        position: 3 
    },
    {
        label: 'Исполнение штока',
        params: [
            {
                value: '',
                label: 'Односторонний',
                text: 'При заказе не указывается'
            },
            {
                value: 'T',
                label: 'Двусторонний',
                text: ''
            }, 
        ],
        key: "stock",
        position: 4 
    },

    {
        label: 'Демпфирование (Торможение)',
        params: [
            {
                value: 'P',
                label: 'Упругое нерегулируемое',
                text: ''
            },
            {
                value: 'PPV',
                label: 'Воздушное регулируемое',
                text: 'только для d 16...63 мм'
            }, 
        ],
        key: "braking",
        position: 4 
    },


    {
        label: 'материал штока',
        params: [
            {
                value: '',
                label: 'сталь 45, покрытие хром',
                text: 'При заказе не указывается'
            },
            {
                value: '304',
                label: 'Нерж. сталь SS304',
                text: ''
            }, 
        ],
        key: "stock_material",
        position: 4 
    },
]