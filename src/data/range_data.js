export let kv_ranges = {
    stock_len_range: {
        label: "Удленение штока",
        max: 500,
        leter: 'E'
    },
    groove_len_range: {
        label: "Удленение резьбы штока",
        max: 35,
        leter: 'L'
    }
} 