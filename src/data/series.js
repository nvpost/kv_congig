export let series = [
    {
        name: "KVNG",
        stock_len_range: {
            min:0,
            max: 500,
        },
        groove_len_range: {
            min:0,
            max: 35,
        },

    },
    {
        name: "KVSC",
        stock_len_range: {
            min:0,
            max: 500,
        },
        groove_len_range: {
            min:0,
            max: 35,
        },
    },
    {
        name: "KVNC",
        stock_len_range: {
            min:0,
            max: 500,
        },
        groove_len_range: {
            min:0,
            max: 35,
        },
    },
    {
        name: "KVBC",
        stock_len_range: {
            min:0,
            max: 500,
        },
        groove_len_range: {
            min:0,
            max: 35,
        },
    },
    {
        name: "KVDN",
        stock_len_range: {
            min:0,
            max: 200,
        },
        groove_len_range: {
            min:0,
            max: 35,
        },
    },
    {
        name: "KVVU",
        stock_len_range: {
            min:0,
            max: 200,
        },
        groove_len_range: {
            min:0,
            max: 35,
        },
    },
    {
        name: "KVDA",
        stock_len_range: {
            min:0,
            max: 200,
        },
        groove_len_range: {
            min:0,
            max: 35,
        },
    },
    {
        name: "KVNU",
        stock_len_range: {
            min:0,
            max: 200,
        },
        groove_len_range: {
            min:0,
            max: 35,
        },
    },
    {
        name: "KVMAL",
        stock_len_range: {
            min:0,
            max: 200,
        },
        groove_len_range: {
            min:0,
            max: 35,
        },
    },
    {
        name: "KVTDN",
        stock_len_range: {

        },
        groove_len_range: {

        },
    },
    {
        name: "KVFM",
        stock_len_range: {

        },
        groove_len_range: {

        },
    },
    {
        name: "KVSW",
        stock_len_range: {

        },
        groove_len_range: {

        },
    },
]