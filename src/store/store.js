import { createStore } from 'vuex'


import {kv_data} from '../data/main_data'
import {diameters} from '../data/diameters'
import {filters} from '../data/filters'
import {series} from '../data/series_flat'
import {kv_ranges} from '../data/range_data'

export default createStore({
  state: {

    //   kv_prepared_data: {}, Убрать
      kv_data: kv_data,
      diameters: diameters,
      bool_filters: filters,
      kv_ranges: kv_ranges,
      series: series,
      config_mode: 0, // Выбор отображения блоков фильтров
      show_legend: false,
      start_data: 1,
      modalOpen: false,

    //   Изменяемые данные
      selectedMods: {},
      filteredMods: [],
      maxHod: 0,
      d_parts: [],
      avSeries: [],
      
    },
    getters: {
    },
    mutations: {
        commitMod(state, payload){
            let key = payload.key
            let value = payload.value

            state.selectedMods[key] = value

            this.commit('doFilteredMods')

        },

        removeMod(state, payload){
            let what = payload.what
            delete state.selectedMods[what]

            this.commit('doFilteredMods')
        },

        doFilteredMods(state){
            state.filteredMods = state.kv_prepared_data
            state.kv_ranges.stock_len_range.max = 0
            state.kv_ranges.groove_len_range.max = 0

            state.filteredMods = state.filteredMods.filter(i=>{
                let return_flags = [];
                var flag = false;

                if(state.selectedMods.seria){
                    flag = i.seria == state.selectedMods.seria
                    return_flags.push(flag)
                }

                if(state.selectedMods.d){
                    flag = i.d == state.selectedMods.d
                    return_flags.push(flag)
                }

                if(state.selectedMods.hod_value ){
                    flag = i.hod_value >= state.selectedMods.hod_value
                    i.hod = state.selectedMods.hod_value
                    return_flags.push(flag)
                }
                if(state.selectedMods.groove || state.selectedMods.groove==''){
                    flag = i.groove == state.selectedMods.groove
                    return_flags.push(flag)
                }
                if(state.selectedMods.stock || state.selectedMods.stock==''){
                    flag = i.stock == state.selectedMods.stock
                    return_flags.push(flag)
                }

                if(state.selectedMods.stock_material || state.selectedMods.stock_material==''){
                    flag = i.stock_material == state.selectedMods.stock_material
                    return_flags.push(flag)    
                }

                if(state.selectedMods.E){
                    console.log(state.selectedMods.E)
                    flag = i.stock_len_range >= state.selectedMods.E
                    i.E = state.selectedMods.E
                    return_flags.push(flag)
                }

                if(state.selectedMods.L){
                    console.log(state.selectedMods.L)
                    flag = i.groove_len_range >= state.selectedMods.L
                    i.L = state.selectedMods.L
                    return_flags.push(flag)
                }

                if(state.selectedMods.braking){
                    flag = i.braking == state.selectedMods.braking
                    return_flags.push(flag)
                }


                state.kv_ranges.stock_len_range.max = i.stock_len_range > state.kv_ranges.stock_len_range.max ? i.stock_len_range : state.kv_ranges.stock_len_range.max
                state.kv_ranges.groove_len_range.max = i.groove_len_range > state.kv_ranges.groove_len_range.max ? i.groove_len_range : state.kv_ranges.groove_len_range.max


                let checker = return_flags.every(v => v === true)

                return checker
            })

            console.log("Найдено модификаций", state.filteredMods.length)
            console.log('state.selectedMods', state.selectedMods)
        },
        setMaxHod(state){
            // console.log('setMaxHod')
            let hod_max_arr = state.filteredMods.reduce(function(prev, cur){
                return (+cur.hod_value > +prev.hod_value) ? cur : prev
            })
            
            state.maxHod = hod_max_arr.hod_value
            // console.log(state.maxHod)

        },
        setD_Parts(state){
            // console.log('setD_Parts')
            let d_parts = []; 
            state.kv_prepared_data.map(i=>{
                i.d_parts.map(d=> {
                    d_parts.includes(d) ? false: d_parts.push(parseInt(d))
                })
                
            })
            d_parts.sort((a, b) => { return a - b})
            state.d_parts = d_parts
        },


        
    },
    actions: {
        checkAvailability_store(state, payload){
            // let key = payload.key
            // let val = payload.val

            console.log(payload)

            return true

            // let avSeries = []
            // state.filteredMods.map(i=>{
            //     avSeries.indexOf(i[key])==-1 ? avSeries.push(i[key]) : false
                
            // })
            // console.log(avSeries.indexOf(val)>-1)
            // return avSeries.indexOf(val)>-1
        }

    },
    modules: {

    }
})