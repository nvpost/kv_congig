import { createApp } from 'vue'
import  store from './store/store'
import App from './App.vue'
import './assets/style.css'

import {funcs} from './funcs.js'

const app = createApp(App)

app.use(store)

app.use(funcs)

app.mount('#app')
